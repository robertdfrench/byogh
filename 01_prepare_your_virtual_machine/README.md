# Prepare your Virtual Machine
*Setting up an empty VM for your git host*

First, let's check to make sure your `aws` command is set up properly:

```bash
aws ec2 describe-instances
```

If this works, it should return some JSON output describing the "Instance Reservations", or just the number of VMs you have running.
If you are using a new account, this list will be empty:

```json
{
    "Reservations": []
}
```

If you are using a shared account (i.e. one that you use at work), you may see a long list of items:

```json
{
    "Reservations": [
        {
            "Instances": [
                {
                    "Monitoring": {
                        "State": "disabled"
                    },
                    "PublicDnsName": "ec2-127-0-0-1.compute-1.amazonaws.com",
                    "StateReason": {
                        "Message": "",
                        "Code": "",
                        ...
```

Either of those outputs indicate success.

If you see an error, it could mean one of two things: either you don't have the [AWS CLI Tool][1] installed correctly, or it may also mean you do not have an [AWS Account][2] set up.
Take the time to work through Amazon's documentation, and come back *only when* the `aws ec2 describe-instances` command above runs without producing an error.

### Set up a temporary instance
Now that your `aws` cli tool is up and running (see [these][1] [docs][2] if not), we will walk through creating a virtual machine on AWS EC2 (frequently called an *instance*).

Before we begin, take a look at [Amazon's Guide for launching EC2 instances from the command line][3].
They outline the three things we need in order to launch an EC2 instance:

* `--image-id`: Amazon Machine Image (a disk image for the operating system we want to launch)
* `--key-name`: An SSH keypair, where we have shared the public key with Amazon
* `--security-group`: The name of a "Security Group" (a policy that says what kind of network traffic should be allowed)

once we have those three items, we will be able to launch an EC2 instance like so:

```bash
# The \ can be used to break up a long line into more readable subsections
aws ec2 run-instances \
	--image-id $IMAGE_ID \
	--count 1 \
	--instance-type t1.micro \
	--key-name $KEY_NAME \
	--security-groups $SECURITY_GROUP
# Bash is old-school, so variables are usually written in $ALL_CAPS
```

#### Find the ImageID for the OS we want
You can build your git host with any OS you want, but for the sake of not getting into any OS wars, we will go with Amazon's own [Amazon Linux 2][4].
You can query information about all of the OS images that Amazon provides, but unfortunately they do a very poor job curating this data, so we will need to clean it up a bit.

Let's begin by using the `aws` tool to download a list of *all* the OS images that Amazon provides:
```bash
aws ec2 describe-images --owners amazon > amazon_images.json
# the > tells the shell to save its output to a file named 'amazon_images.json'
```

You may notice that it takes a minute or so to retrieve this list.
It is huge, and mostly full of windows stuff.
Once you have it, you can use the `cat` command to print the contents to your terminal:

```bash
# 'cat' is short for 'concatenate', which means 'concatenate to the terminal'
cat amazon_images.json
```

I mean is that a lot of stuff or what?
You can see the size of this file by running:

```bash
# -l gives 'long' output, and -h gives 'human readable' filesizes
ls -lh amazon_images.json
# The use of -l and -h together as '-lh' is a common UNIX convention
```

When I ran this, it was about 18 Megabytes, but that may change as Amazon adds new images and removes old ones.

Now, the needle we are looking for is the most recent version of Amazon Linux 2.
To help us find that needle in this multi-megabyte JSON haystack, we can leverage a tool called [jq][5]:

```bash
jq '.Images | length' amazon_images.json
```

`jq` takes two arguments: a search query and a file containing a bunch of json.
In this case, our query says to find the length of the `.Images` array defined in the `amazon_images.json` file.
At the time of writing, this is about 6300 images, but you may get a different number depending on changes in Amazon's curation policies.

##### Finding the latest Amazon Linux 2 image
Now that we have a list of images on hand, let's see if we can narrow this down to just what we want: those named "Amazon Linux 2".
Unfortunately, Amazon has made this difficult for us as *not all of the images are named*.
For example:

```bash
jq '.Images | map(select(. | has("Description"))) | length' amazon_images.json
```

This tells `jq` to select items from the `.Images` array where the items have a field called `"Description"`.
If you are like me, you will be chagrined to notice that there are somewhat fewer *named* images than there are images in total.
Not to worry, as `jq` can just as easily weed out these unnamed images like so:

```bash
jq '.Images | map(select(. | has("Description")))' amazon_images.json > named_images.json
```

Here, instead of counting the images, we just have `jq` print them, and we again leverage bash's `>` symbol to save that output to a file called `named_image.json`.
Now that we have a file containing only items that are reliably named, we can use `jq` again to select only those with the string "Amazon Linux 2 AMI" in their description:

```bash
jq  'map(select(.Description | contains("Amazon Linux 2 AMI")))' named_images.json > amzn2_images.json
```

We can get a glimpse of what is remaining in this file by asking `jq` to print only the description for each image:

```bash
jq '.[].Description' amzn2_images.json
```

That means "Show the Description for each element in the array".
If we wanted to get more information about a specific element in that list, say the fifth one, we could ask `jq` to print only that element like so:

```bash
jq '.[5]' amzn2_images.json
```

You may see that some of the images are named "minimal", and some are named "gp2".
For the sake of our exercises, there will be little difference between any of the remaining images.
But `jq` makes it easy enough to be precise.
Those marked "minimal" will have fewer initial packages installed, so we can install only the ones we want.
Those marked "gp2" will be better suited for a faster storage type that we won't use here, because there is no need to spend the money.
Thus, what we want is the *latest* version of the *minimal* image that uses the *standard* (not gp2) storage type:

```bash
# I mean is this a whopper of a command or what?
jq 'map(select(.BlockDeviceMappings[0].Ebs.VolumeType == "standard")) | map(select(.Description | contains("Minimal"))) | sort_by(.CreationDate)[-1]' amzn2_images.json
```

And now to clean up your workspace:

```bash
# Bash will expand '*' to match any file with the .json extension
rm -f *.json
```


[1]: https://docs.aws.amazon.com/cli/latest/userguide/installing.html
[2]: https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/
[3]: https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-launch.html#run-ec2-classic
[4]: https://aws.amazon.com/amazon-linux-2/
[5]: https://stedolan.github.io/jq/
