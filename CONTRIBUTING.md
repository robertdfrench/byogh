# Contributing
Rather than emphasizing pull requests, I would strongly encourage you to fork
this repository and make the changes you see fit.

I will be happy to look at any pull requests that are submitted, but I have very
poorly defined philosophical goals for this project. Rather than rejecting a good
PR because it doesn't meet needs that I haven't articulated (or am still
discovering), I would prefer to see folks succeed on their own terms [in the
style of Illumos](https://www.youtube.com/watch?v=-zRN7XLCRhc).
