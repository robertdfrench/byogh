# Build Your Own Git Host
*UNIX Essentials for the Working Programmer*

This tutorial will walk you through building a functioning git host from
scratch. The git and command-line skills covered in these exercises will help
you improve automation in your *real* projects. It will also help you leverage
tools such as Jenkins and GitHub by showing what's going on under the hood.

### Goals
* Authenticate over SSH
* Render status pages
* Merge or Reject Pull Requests
* Built-in CI tests
* Easy backup using git+ssh

### Requirements
A stinkin AWS account. I firmly believe that Jeff Bezos should not own the
internet, and will add other cloud provider options soon. And VirtualBox. But it
is easiest to start with the devil.

### Copyright
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Build Your Own Git Host</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.robertdfrench.me" property="cc:attributionName" rel="cc:attributionURL">Robert D. French</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/robertdfrench/byogh" rel="dct:source">gitlab.com/robertdfrench/byogh</a>.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Bear Icon</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.creativetail.com/" property="cc:attributionName" rel="cc:attributionURL">Creative Tail</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://www.brandeps.com/icon/B/Bear-01" rel="dct:source">https://www.brandeps.com/icon/B/Bear-01</a>.
